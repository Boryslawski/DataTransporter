﻿using DataTransporter.Models;
using DataTransporter.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DataTransporter.Controllers
{
    [Route("api/[controller]")]
    public class DataController : Controller
    {
        DataService _dataService;

        public DataController(DataService service)
        {
            _dataService = service;
        }

        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_dataService.getList());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(_dataService.getById(id));
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
