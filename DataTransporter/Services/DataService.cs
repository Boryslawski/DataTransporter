﻿using System.Collections.Generic;
using System.Linq;
using DataTransporter.Data;
using DataTransporter.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DataTransporter.Services
{
    public class DataService
    {
        private Context _context;

        public DataService(Context context)
        {
            _context = context;
        }

        public List<DataModel> getList()
        {
            return _context.Datas.ToList();
        }

        public DataModel getById(int id)
        {
            return _context.Datas.SingleOrDefault(x => x.Id == id);
        }

    }
}
