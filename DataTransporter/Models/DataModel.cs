﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataTransporter.Models
{
    public class DataModel
    {
        [Key]
        public int Id { get; set; }
        public String Value { get; set; }
    }
}
